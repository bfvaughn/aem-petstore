import { MapTo } from '@adobe/aem-angular-editable-components';
import { Component, OnInit, Input } from '@angular/core';
import { GraniteI18nHelper } from '../../../utils/GraniteI18nHelper'

const HelloWorldEditConfig = {
    emptyLabel: 'HelloWorld',
    isEmpty: cqModel =>
        !cqModel || !cqModel.text || cqModel.text.trim().length < 1
};

@Component({
  selector: 'app-helloworld',
  templateUrl: './helloworld.component.html',
  styleUrls: ['./helloworld.component.css']
})
export class HelloWorldComponent implements OnInit {

  @Input() text: string;
  @Input() message: string;

  constructor(private graniteUtils: GraniteI18nHelper) { }

  ngOnInit(): void {
  }

  get textI18n(): string {
    return this.graniteUtils.getI18nText(this.text);
  }
}

MapTo('petstore-ang/components/helloworld/v1/helloworld')(HelloWorldComponent, HelloWorldEditConfig );
