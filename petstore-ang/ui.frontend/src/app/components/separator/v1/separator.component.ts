import { Component, OnInit, Input, HostBinding } from '@angular/core';

export const SeparatorEditConfig = {
    emptyLabel: 'Separator',
    isEmpty: cqModel =>
        !cqModel
};

@Component({
  selector: 'app-separator',
  templateUrl: './separator.component.html',
  styleUrls: ['./separator.component.scss']
})
export class SeparatorComponent implements OnInit {

  @Input() appliedCssClassNames: string;

  constructor() { }

  ngOnInit(): void {
  }

}
