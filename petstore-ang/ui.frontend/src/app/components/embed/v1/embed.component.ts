import { Component, Input, OnInit, AfterViewInit, ViewChild, TemplateRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

export const EmbedEditConfig = {
    emptyLabel: 'Embed',
    isEmpty: cqModel =>
        !cqModel || !cqModel.type || cqModel.type.trim().length < 1
};

@Component({
  selector: 'app-embed',
  templateUrl: './embed.component.html',
  styleUrls: ['./embed.component.css']
})
export class EmbedComponent implements OnInit, AfterViewInit {

  @Input() id: string;
  @Input() type: string;
  @Input() url: string;
  @Input() html: string;

  embedTemplate: TemplateRef<any>;

  @ViewChild('embedHtml')
  private embedHtmlTemplate: TemplateRef<any>;

  @ViewChild('embedUrl')
  private embedUrlTemplate: TemplateRef<any>;

  @ViewChild('embedEmbeddable')
  private embedEmbeddableTemplate: TemplateRef<any>;

  constructor( private sanitizer: DomSanitizer ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {

    if ( this.type === 'HTML' ) {
      this.embedTemplate = this.embedHtmlTemplate;
    } else if ( this.type === 'URL' ) {
      this.embedTemplate = this.embedUrlTemplate;
    } else if ( this.type === 'EMBEDDABLE' ) {
      this.embedTemplate = this.embedEmbeddableTemplate;
    }

  }

  get htmlEmbed() {
    return this.sanitizer.bypassSecurityTrustHtml(this.html);
  }

}

