import { Component, OnInit, Input, AfterViewInit, ViewChild, TemplateRef } from '@angular/core';
import { Link } from '../../../models/v1/link';

export const ButtonEditConfig = {
    emptyLabel: 'Button',
    isEmpty: cqModel =>
        !cqModel || !cqModel.buttonLink || !cqModel.buttonLink.url || cqModel.buttonLink.url.trim().length < 1
};

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit, AfterViewInit {

  @Input() id: string;
  @Input() buttonLink: Link;
  @Input() appliedCssClassNames: string;

  buttonTemplate: TemplateRef<any>;

  @ViewChild('staticLink')
  private staticLinkTemplate: TemplateRef<any>;

  @ViewChild('routedLink')
  private routedLinkTemplate: TemplateRef<any>;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {

    console.log(this.buttonLink);
    if ( this.buttonLink && this.buttonLink.routed === true ) {
      this.buttonTemplate = this.routedLinkTemplate;
    } else {
      this.buttonTemplate = this.staticLinkTemplate;
    }

  }

}
