/**
 * Helper function to access the AEM i18n translations
 */
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GraniteI18nHelper {

  getI18nText(key: string) : string {
  // Need to see how to access Granite Utils in Angular
    if (key && window.Granite && window.Granite.I18n) {
      return window.Granite.I18n.get(key);
    }
    return key;
  }

}