import { Input } from '@angular/core';

export class Link {
  @Input() url: string;
  @Input() routed: boolean;
  @Input() title: string;
  @Input() label: string;
  @Input() valid: boolean;
}