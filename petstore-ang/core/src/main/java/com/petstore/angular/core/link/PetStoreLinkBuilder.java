package com.petstore.angular.core.link;

import com.day.cq.wcm.api.PageManager;
import org.apache.sling.api.resource.Resource;

public class PetStoreLinkBuilder {

    private String staticLink;
    private String routedLink;
    private String title;
    private String label;
    private String target;
    private Resource resource;
    private PageManager pageManager;

    public PetStoreLinkBuilder(Resource resource) {
        this.resource = resource;
        this.pageManager = resource.getResourceResolver().adaptTo(PageManager.class);
    }

    public PetStoreLinkBuilder withPath(String staticLink) {
        this.staticLink = staticLink;
        return this;
    }

    public PetStoreLinkBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public PetStoreLinkBuilder withLabel(String label) {
        this.label = label;
        return this;
    }

    public PetStoreLinkBuilder withTarget(String target) {
        this.target = target;
        return this;
    }

    public PetStoreLink build() {
        buildRouterLink();
        return new PetStoreLink(this.staticLink, this.routedLink, this.title, this.label, this.target);
    }

    private void buildRouterLink() {
        if (pageManager.getPage(this.staticLink) != null) {
            this.routedLink = this.resource.getResourceResolver().resolve(this.staticLink).getPath();
        }
    }
}
