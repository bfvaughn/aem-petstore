package com.petstore.angular.core.link;

import com.adobe.cq.wcm.core.components.commons.link.Link;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.StringUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PetStoreLink implements Link {

    private final String url;
    private final boolean routed;
    private final String target;
    private final String title;
    private final String label;

    public PetStoreLink(String staticLink, String routedLink, String title, String label, String target) {
        this.title = title;
        this.label = label;
        this.target = target;

        if (StringUtils.isNotBlank(routedLink)) {
            this.url = routedLink;
            this.routed = true;
        } else {
            this.url = staticLink;
            this.routed = false;
        }
    }

    public boolean isValid() {
        return this.url != null;
    }

    public boolean isRouted() {
        return this.routed;
    }

    public String getUrl() {
        return url;
    }

    public String getTarget() {
        return target;
    }

    public String getTitle() {
        return title;
    }

    public String getLabel() {
        return label;
    }
}
