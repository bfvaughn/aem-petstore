# Lessons Learned
These are lessons learned based on building Angular SPA using AEM Archetype version 30 
and version 6.5.8 of AEM. 

## Image Component
Needed to not use the Core Image component because the model would 
throw exceptions when the Model generated the JSON.  My class is just 
wrapping the Core Image component and calling its getter methods.

###Core Component Composition
```
    @Self
    @Via(type = ResourceSuperType.class)
    private Image image;
```

## Button Component
The Core Button class is not generating Router Links correctly. To
fix this I have implemented my onw Button that wraps the Core Button Component. 
This wrapper also provides a wrapper class for Link to generate the correct
angular link properties.

###Button Link Wrapping
```
    @Override
    public Link getButtonLink() {
        PetStoreLink build = new PetStoreLinkBuilder(request.getResource())
                .withPath(this.link)
                .withTitle(button.getText())
                .build();
        return build;
    }
```

###Button Model JSON
```
"button": {
    "accessibilityLabel": "button label",
    "buttonLink": {
        "url": "/content/petstore-ang/us/en/home",
        "routed": true,
        "title": "Learn more",
        "valid": true
    },
    "id": "btn-id",
    "text": "Learn more",
    "appliedCssClassNames": "btn-line",
    "dataLayer": {
        "btn-id": {
            "@type": "core/wcm/components/button/v1/button",
            "repo:modifyDate": "2021-11-12T18:09:42Z",
            "dc:title": "Learn more"
        }
    },
    ":type": "petstore-ang/components/button"
}
```

##Style System
There looks to be some support for the Style System in SPA Editor components
but not on all components.  The Image core component doesn't work while the Text does.
I believe the issue is with how the components are built.  Seems you can't style
imported components from aem-core-components-angular-base.

####Does not work for Image component
```
<core-image-v2 class="cmp-padding">
```

####Does work for Text component
```
<app-text _nghost-tly-c48="" class="cmp-padding" data-rte-editelement="true">
```