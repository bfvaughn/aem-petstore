/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.petstore.htl.core.models.v1;

import com.adobe.cq.export.json.ComponentExporter;
import com.petstore.base.core.models.HelloWorld;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.via.ResourceSuperType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = SlingHttpServletRequest.class,
    adapters = { HelloWorld.class, ComponentExporter.class },
    resourceType = HelloWorldImpl.RESOURCE_TYPE,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HelloWorldImpl implements HelloWorld {

    static final String RESOURCE_TYPE = "petstore-htl/components/helloworld/v1/helloworld";

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Self
    @Via(type = ResourceSuperType.class)
    private HelloWorld helloWorld;

    @Override
    public String getMessage() {
        return this.helloWorld.getMessage();
    }

    @Override
    public String getText() {
        return this.helloWorld.getText();
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }
}
