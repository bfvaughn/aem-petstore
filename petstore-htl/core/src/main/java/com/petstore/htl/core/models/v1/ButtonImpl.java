package com.petstore.htl.core.models.v1;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.adobe.cq.wcm.core.components.commons.link.Link;
import com.adobe.cq.wcm.core.components.models.Button;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.via.ResourceSuperType;

@Model(adaptables = SlingHttpServletRequest.class, adapters = { Button.class,
    ComponentExporter.class }, resourceType = ButtonImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class ButtonImpl implements Button {
    static final String RESOURCE_TYPE = "petstore-htl/components/button/v1/button";

    @Self
    private SlingHttpServletRequest request;

    @Self
    @Via(type = ResourceSuperType.class)
    private Button button;

    @Override
    public String getText() {
        return this.button.getText();
    }

    @Override
    public Link getButtonLink() {
        return this.button.getButtonLink();
    }

    @Override
    public String getLink() {
        return this.button.getLink();
    }

    @Override
    public String getIcon() {
        return this.button.getIcon();
    }

    @Override
    public String getAccessibilityLabel() {
        return this.button.getAccessibilityLabel();
    }
}
