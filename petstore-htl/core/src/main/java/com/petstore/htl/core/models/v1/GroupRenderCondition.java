package com.petstore.htl.core.models.v1;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.wcm.core.components.models.Component;
import com.adobe.granite.ui.components.rendercondition.RenderCondition;
import com.adobe.granite.ui.components.rendercondition.SimpleRenderCondition;
import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Model(adaptables = SlingHttpServletRequest.class,
    adapters = { GroupRenderCondition.class, ComponentExporter.class },
    resourceType = GroupRenderCondition.RESOURCE_TYPE,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class GroupRenderCondition implements Component {

    static final String RESOURCE_TYPE = "petstore-htl/components/rendercondition/group/v1/group";
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Self
    private SlingHttpServletRequest request;

    @ValueMapValue
    private List<String> allowedGroups;

    @PostConstruct
    public void init() {

        logger.info("....GroupRenderCondition.....");
        //Lookup user and get their groups
        ResourceResolver resourceResolver = request.getResourceResolver();
        boolean show = false;
        Session session = resourceResolver.adaptTo(Session.class);

        try {
            UserManager userManager = ((JackrabbitSession) session).getUserManager();
            Iterator<Authorizable> groupIterator = userManager.findAuthorizables("jcr:primaryType", "rep:Group");
            while (groupIterator.hasNext()) {

                logger.info("Getting group");

                Authorizable group = groupIterator.next();

                if (group.isGroup()) {
                    logger.info("Group found {}", group.getID());
                    if (allowedGroups.contains(group.getID())) {
                        show = true;
                        break;
                    }
                }
            }
        } catch (RepositoryException e) {
            logger.error(e.getMessage(), e);
        }

        // Add the render condition
        request.setAttribute(RenderCondition.class.getName(), new SimpleRenderCondition(show));
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }
}

