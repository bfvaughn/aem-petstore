package com.petstore.htl.core.models;

import io.wcm.testing.mock.aem.junit5.AemContext;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;

public class ParentAemContentTest {
    protected MockSlingHttpServletRequest getMockSlingHttpServletRequest(AemContext context,
            String textContentPath, String contentPath, String componentPath) {

        context.load().json(textContentPath, contentPath);
        context.currentResource(contentPath + componentPath);
        MockSlingHttpServletRequest request = context.request();
        return request;
    }
}
