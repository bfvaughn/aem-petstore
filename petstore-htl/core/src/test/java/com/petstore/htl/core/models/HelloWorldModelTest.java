/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.petstore.htl.core.models;

import com.petstore.base.core.models.HelloWorld;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Simple JUnit test verifying the HelloWorldModel
 */
@ExtendWith(AemContextExtension.class)
class HelloWorldModelTest extends ParentAemContentTest{

    public static final String TEST_CONTENT = "/models/helloworld/helloworld-content.json";
    public static final String CONTENT_PATH = "/content/petstore-htl/us/en/home/demo";
    public static final String COMPONENT_PATH = "/jcr:content/root/container/container/helloworld";
    private HelloWorld hello;

    @BeforeEach
    public void setup(AemContext context) throws Exception {

        MockSlingHttpServletRequest request = getMockSlingHttpServletRequest(context, TEST_CONTENT, CONTENT_PATH, COMPONENT_PATH);
        // create sling model
        hello = request.adaptTo(HelloWorld.class);
    }



    @Test
    void testGetMessage() throws Exception {
        // some very basic junit tests
        // Can't inject parent model
//        String msg = hello.getMessage();
        assertNotNull(hello);
    }

}
