/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.petstore.base.core.models;

import com.petstore.base.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Simple JUnit test verifying the HelloWorldModel
 */
@ExtendWith(AemContextExtension.class)
class HelloWorldModelTest {

    public static final String TEST_CONTENT = "/models/helloworld/helloworld-content.json";
    public static final String CONTENT_PATH = "/content/petstore-base/us/en/home/demo";
    public static final String COMPONENT_ROOT_PATH = "/jcr:content/root/container/container";
    private HelloWorld hello;
    private AemContext context = AppAemContext.newAemContext();

    @BeforeEach
    public void setup() throws Exception {
        this.context.load().json(TEST_CONTENT, CONTENT_PATH);
    }

    private HelloWorld getComponent(String componentPath) {
        context.currentResource(CONTENT_PATH + componentPath);
        MockSlingHttpServletRequest request = context.request();
        return request.adaptTo(HelloWorld.class);
    }


    @Test
    void testGetMessage() throws Exception {
        // some very basic junit tests
        this.hello = getComponent(COMPONENT_ROOT_PATH + "/helloworld");
        String msg = hello.getMessage();
        assertNotNull(msg);
        assertEquals("current-page", hello.getMessage());
    }

    @Test
    void testGetText() throws Exception {
        // some very basic junit tests
        this.hello = getComponent(COMPONENT_ROOT_PATH + "/helloworld_2");
        String msg = hello.getText();
        assertNotNull(msg);
        assertEquals("Hello World", hello.getText());
    }

}
