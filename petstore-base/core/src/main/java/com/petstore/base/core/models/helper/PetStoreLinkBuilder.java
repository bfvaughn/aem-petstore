package com.petstore.base.core.models.helper;

import com.day.cq.wcm.api.PageManager;
import org.apache.sling.api.resource.Resource;

public class PetStoreLinkBuilder {

    private String path;
    private String routedLink;
    private String title;
    private String label;
    private String target;
    private Resource resource;
    private PageManager pageManager;

    public PetStoreLinkBuilder(Resource resource) {
        this.resource = resource;
        this.pageManager = resource.getResourceResolver().adaptTo(PageManager.class);
    }

    public PetStoreLinkBuilder withPath(String path) {
        this.path = path;
        return this;
    }

    public PetStoreLinkBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public PetStoreLinkBuilder withLabel(String label) {
        this.label = label;
        return this;
    }

    public PetStoreLinkBuilder withTarget(String target) {
        this.target = target;
        return this;
    }

    public PetStoreLink build() {
        buildRouterLink();
        return new PetStoreLink(this.path, this.routedLink, this.title, this.label, this.target);
    }

    private void buildRouterLink() {
        if (pageManager.getPage(this.path) != null) {
            this.routedLink = this.resource.getResourceResolver().map(this.path);
        }
    }
}
