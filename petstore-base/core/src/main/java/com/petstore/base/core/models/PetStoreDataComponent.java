package com.petstore.base.core.models;

import com.adobe.cq.wcm.core.components.models.datalayer.ComponentData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class PetStoreDataComponent implements ComponentData {
    private final Logger logger = LoggerFactory.getLogger(AbstractPetstoreComponent.class);

    private final String id;
    private final String type;
    private final Date lastModifiedDate;
    private final String parentId;
    private final String title;
    private final String description;
    private final String text;
    private final String linkUrl;

    PetStoreDataComponent(Builder builder) {
        this.id = builder.id;
        this.type = builder.type;
        this.lastModifiedDate = builder.lastModifiedDate;
        this.parentId = builder.parentId;
        this.title = builder.title;
        this.description = builder.description;
        this.text = builder.text;
        this.linkUrl = builder.description;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public Date getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    @Override
    public String getParentId() {
        return this.parentId;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public String getText() {
        return this.text;
    }

    @Override
    public String getLinkUrl() {
        return this.linkUrl;
    }

    @Override
    public String getJson() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            logger.error("Failed to generate DataLayer JSON string", e);
            return null;
        }
    }

    public static class Builder {
        private String id;
        private String type;
        private Date lastModifiedDate;
        private String parentId;
        private String title;
        private String description;
        private String text;
        private String linkUrl;

        public Builder(String type) {
            this.type = type;
        }

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder withLastModifiedDate(Date lastModifiedDate) {
            this.lastModifiedDate = lastModifiedDate;
            return this;
        }

        public Builder withParentId(String parentId) {
            this.parentId = parentId;
            return this;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withLinkUrl(String linkUrl) {
            this.linkUrl = linkUrl;
            return this;
        }

        public Builder withText(String text) {
            this.text = text;
            return this;
        }

        public PetStoreDataComponent build() {
            return new PetStoreDataComponent(this);
        }
    }
}
