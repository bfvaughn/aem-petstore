package com.petstore.base.core.models.helper;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import org.apache.sling.api.SlingHttpServletRequest;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class GraniteI18nHelper {

    public static String getText(Page currentPage, SlingHttpServletRequest slingHttpServletRequest, String key) {
        Locale pageLang = currentPage.getLanguage(false);
        ResourceBundle resourceBundle = slingHttpServletRequest.getResourceBundle(pageLang);
        I18n i18n = new I18n(resourceBundle);
        return i18n.get(key);
    }

    public static String getText(Page currentPage, SlingHttpServletRequest slingHttpServletRequest, String key, String...args) {
        Locale pageLang = currentPage.getLanguage(false);
        ResourceBundle resourceBundle = slingHttpServletRequest.getResourceBundle(pageLang);
        I18n i18n = new I18n(resourceBundle);
        return MessageFormat.format(i18n.get(key), args);
    }
}
