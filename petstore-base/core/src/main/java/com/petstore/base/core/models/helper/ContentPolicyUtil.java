package com.petstore.base.core.models.helper;

import com.day.cq.wcm.api.policies.ContentPolicy;
import com.day.cq.wcm.api.policies.ContentPolicyManager;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

import java.util.HashMap;
import java.util.Map;

public class ContentPolicyUtil {

    public static Map<String, String> getComponentStyles(Resource resource) {
        Map<String, String> styles = new HashMap();

        ResourceResolver resourceResolver = resource.getResourceResolver();
        ContentPolicyManager policyManager = resourceResolver.adaptTo(ContentPolicyManager.class);
        ContentPolicy contentPolicy = policyManager.getPolicy(resource);

        if (contentPolicy != null) {
            Resource styleGroups = resourceResolver.getResource(contentPolicy.getPath() + "/" + "cq:styleGroups");
            if (styleGroups != null) {
                styleGroups.getChildren().forEach((Resource styleGroup) -> {
                    styleGroup.getChild("cq:styles").getChildren().forEach((Resource style) -> {
                        if (style != null) {
                            ValueMap props = style.getValueMap();
                            styles.put(props.get("cq:styleId", String.class), props.get("cq:styleClasses", ""));
                        }
                    });
                });
            }
        }
        return styles;
    }
}
