package com.petstore.base.core.models;

import com.adobe.cq.wcm.core.components.models.Component;
import com.adobe.cq.wcm.core.components.models.datalayer.ComponentData;
import com.adobe.cq.wcm.core.components.util.ComponentUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public abstract class AbstractPetstoreComponent implements Component {

    private final Logger logger = LoggerFactory.getLogger(AbstractPetstoreComponent.class);

    @SlingObject
    protected Resource resource;

    private PetStoreDataComponent petstoreDataComponent;


    public String getDataAsString() {
        String data = null;

        try {
            if (ComponentUtils.isDataLayerEnabled(resource)) {
                Map<String, Object> properties = new HashMap<>();
                properties.put("@type", resource.getResourceType());
                properties.put("repo:modifyDate", "2021-11-11T20:15:50Z");
                properties.put("dc:title", "Learn more");
                properties.put("dc:description", null);
                properties.put("xdm:linkURL", "/content/petstore-ang/us/en/home/dogs/dog-toys.html");
                properties.put("xdm:text", "");
                data = new ObjectMapper().writeValueAsString(properties);
            }
        } catch (JsonProcessingException e) {
            logger.error("Failed to generate DataLayer JSON string", e);
        }

        return data;
    }

    public ComponentData getData() {

        if (ComponentUtils.isDataLayerEnabled(resource)) {
            this.petstoreDataComponent = getComponentData();
        }
        return this.petstoreDataComponent;
    }

    protected PetStoreDataComponent getComponentData() {
        this.petstoreDataComponent = new PetStoreDataComponent.Builder(resource.getResourceType())
                .withLastModifiedDate(
                        (Date)Optional.ofNullable(this.resource.getValueMap().get("jcr:lastModified", Calendar.class)).map(Calendar::getTime).orElseGet(() -> {
                            return (Date)Optional.ofNullable(this.resource.getValueMap().get("jcr:created", Calendar.class)).map(Calendar::getTime).orElse((Date) null);
                        })
                ).build();

        return petstoreDataComponent;
    }

}
