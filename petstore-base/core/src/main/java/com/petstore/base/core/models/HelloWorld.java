package com.petstore.base.core.models;

import com.adobe.cq.wcm.core.components.models.Component;

public interface HelloWorld extends Component {

    String getMessage();

    String getText();
}
