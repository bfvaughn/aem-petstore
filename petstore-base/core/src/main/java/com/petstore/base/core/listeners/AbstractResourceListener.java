package com.petstore.base.core.listeners;

import com.day.cq.replication.ReplicationAction;
import com.day.cq.replication.ReplicationActionType;
import org.apache.sling.api.SlingConstants;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractResourceListener implements EventHandler {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public void handleEvent(final Event event) {
        logger.debug("Resource event: {} at: {}", event.getTopic(), event.getProperty(SlingConstants.PROPERTY_PATH));
        ReplicationAction replicationAction = ReplicationAction.fromEvent(event);
        if (replicationAction != null) {
            ReplicationActionType type = replicationAction.getType();
            for (final String path : replicationAction.getPaths()) {
                handleEvent(type, path);
            }
        }
    }

    private void handleEvent(final ReplicationActionType type, final String path) {
        if (type.equals(ReplicationActionType.ACTIVATE)) {
            logger.debug("handling activate event for path = {}", path);
            handleActivate(path);
        } else if (type.equals(ReplicationActionType.DEACTIVATE)) {
            logger.debug("handling deactivate event for path = {}", path);
            handleDeactivate(path);
        } else if (type.equals(ReplicationActionType.DELETE)) {
            logger.debug("handling delete event for path = {}", path);
            handleDelete(path);
        } else {
            logger.trace("replication action type = {} not handled for path = {}", type, path);
        }
    }

    protected abstract void handleActivate(final String path);

    protected abstract void handleDeactivate(final String path);

    protected abstract void handleDelete(final String path);
}
