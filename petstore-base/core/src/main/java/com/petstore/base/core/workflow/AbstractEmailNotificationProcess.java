package com.petstore.base.core.workflow;

import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;
import com.adobe.granite.security.user.UserPropertiesService;
import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractEmailNotificationProcess implements WorkflowProcess {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Reference
    private MessageGatewayService messageGatewayService;

    @Reference
    private UserPropertiesService userPropertiesService;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    private String PROCESS_ARGS = "PROCESS_ARGS";

    public void sendEmail(WorkflowSession workflowSession, WorkItem workItem, SimpleEmail email) throws EmailException, RepositoryException, LoginException {

        MessageGateway<SimpleEmail> messageGateway = messageGatewayService.getGateway(SimpleEmail.class);
        String emailAddress = getEmailAddress(workflowSession, workItem);
        email.addTo(emailAddress);
        messageGateway.send(email);

    }

    private String getEmailAddress(WorkflowSession workflowSession, WorkItem workItem) {
        String emailAddress = null;
        final Map<String, Object> authInfo = new HashMap<String, Object>();
        authInfo.put(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, workflowSession.getSession());
        try (ResourceResolver resourceResolver = resourceResolverFactory.getResourceResolver(authInfo) ) {
            UserPropertiesManager userPropertiesManager = userPropertiesService.createUserPropertiesManager(resourceResolver);

            String initiator = workItem.getWorkflow().getInitiator();
            UserProperties properties = userPropertiesManager.getUserProperties(initiator, "profile");
            emailAddress = properties.getProperty("email");
        } catch (LoginException | RepositoryException e) {
            logger.error("Failed to get user email address", e);
        }


        return emailAddress;
    }
}
