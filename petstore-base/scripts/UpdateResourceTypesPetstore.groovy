import com.day.cq.wcm.api.components.ComponentManager

def componentManager = resourceResolver.adaptTo(ComponentManager)

def oldResourceType = "petstore-htl/components/button"

def newResourceType = "petstore-htl/components/button/v1/button"

def contentPath = "/content/petstore-htl"

def count = 0

getPage(contentPath).recurse { page ->
    def content = page.node

    content?.recurse { node ->
        def resourceType = node.get("sling:resourceType")

        if (resourceType && resourceType == oldResourceType) {
            node.set("sling:resourceType", newResourceType);
            count++
        }
    }
}

save()

print("Updated $count componetns");