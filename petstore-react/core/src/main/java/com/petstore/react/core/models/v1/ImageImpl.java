package com.petstore.react.core.models.v1;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.adobe.cq.wcm.core.components.models.Image;
import com.adobe.cq.wcm.core.components.models.ImageArea;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.via.ResourceSuperType;

import java.util.List;

@Model(adaptables = SlingHttpServletRequest.class, adapters = { Image.class,
        ComponentExporter.class }, resourceType = ImageImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class ImageImpl implements Image {
    static final String RESOURCE_TYPE = "petstore-react/components/image";

    @Self
    @Via(type = ResourceSuperType.class)
    private Image image;

    @Override
    public String getId() {
        return image != null ? image.getId() : null;
    }

    @Override
    public String getAlt() {
        return image != null ? image.getAlt() : null;
    }

    @Override
    public String getTitle() {
        return image != null ? image.getTitle() : null;
    }

    @Override
    public String getSrc() {
        return image != null ? image.getFileReference() : null;
    }

    @Override
    public String getUuid() {
        return image != null ? image.getUuid() : null;
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }

    @Override
    public String getAppliedCssClasses() {
        return image != null ? image.getAppliedCssClasses() : null;
    }

    @Override
    public List<ImageArea> getAreas() {
        return null;
    }

}
