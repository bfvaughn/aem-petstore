package com.petstore.react.core.models.v1;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.adobe.cq.wcm.core.components.commons.link.Link;
import com.adobe.cq.wcm.core.components.models.ListItem;
import com.adobe.cq.wcm.core.components.models.Teaser;
import com.adobe.cq.wcm.core.components.models.datalayer.ComponentData;
import com.petstore.react.core.models.Card;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.via.ResourceSuperType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Model(adaptables = SlingHttpServletRequest.class, adapters = { Card.class,
        ComponentExporter.class }, resourceType = CardImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class CardImpl implements Card {
    static final String RESOURCE_TYPE = "petstore-react/components/card";

    @Self
    @Via(type = ResourceSuperType.class)
    private Teaser teaser;

    @ValueMapValue
    private String iconImage = "false";


    @Override
    public String getIconImage() {
        return iconImage;
    }

    @Override
    public boolean isActionsEnabled() {
        return this.teaser.isActionsEnabled();
    }

    @Override
    public List<ListItem> getActions() {
        return this.teaser.getActions();
    }

    @Override
    public @Nullable Link getLink() {
        return this.teaser.getLink();
    }

    @Override
    public String getLinkURL() {
        return this.teaser.getLinkURL();
    }

    @Override
    public Resource getImageResource() {
        return this.teaser.getImageResource();
    }

    @Override
    public boolean isImageLinkHidden() {
        return this.teaser.isImageLinkHidden();
    }

    @Override
    public String getPretitle() {
        return this.teaser.getPretitle();
    }

    @Override
    public String getTitle() {
        return this.teaser.getTitle();
    }

    @Override
    public boolean isTitleLinkHidden() {
        return this.teaser.isTitleLinkHidden();
    }

    @Override
    public String getDescription() {
        return this.teaser.getDescription();
    }

    @Override
    public String getTitleType() {
        return this.teaser.getTitleType();
    }

    @Override
    public @Nullable String getId() {
        return this.teaser.getId();
    }

    @Override
    public @Nullable ComponentData getData() {
        return this.teaser.getData();
    }

    @Override
    public @Nullable String getAppliedCssClasses() {
        return this.teaser.getAppliedCssClasses();
    }

    @Override
    public @NotNull String getExportedType() {
        return RESOURCE_TYPE;
    }
}
