package com.petstore.react.core.models.v1;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.adobe.cq.wcm.core.components.models.Carousel;
import com.adobe.cq.wcm.core.components.models.ListItem;
import com.adobe.cq.wcm.core.components.models.datalayer.ComponentData;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.via.ResourceSuperType;

import java.util.List;
import java.util.Map;

@Model(adaptables = SlingHttpServletRequest.class, adapters = {Carousel.class,
        ComponentExporter.class}, resourceType = CarouselImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class CarouselImpl implements Carousel {

    static final String RESOURCE_TYPE = "petstore-react/components/carousel/v1/carousel";

    @Self
    @Via(type = ResourceSuperType.class)
    private Carousel carousel;

    @Override
    public boolean getAutoplay() {
        return this.carousel.getAutoplay();
    }

    @Override
    public Long getDelay() {
        return this.carousel.getDelay();
    }

    @Override
    public boolean getAutopauseDisabled() {
        return this.carousel.getAutopauseDisabled();
    }

    @Override
    public String getAccessibilityLabel() {
        return this.carousel.getAccessibilityLabel();
    }

    @Override
    public boolean isControlsPrepended() {
        return this.carousel.isControlsPrepended();
    }

    @Override
    public List<ListItem> getItems() {
        return this.carousel.getItems();
    }

    @Override
    public String getBackgroundStyle() {
        return this.carousel.getBackgroundStyle();
    }

    @Override
    public Map<String, ? extends ComponentExporter> getExportedItems() {
        return this.carousel.getExportedItems();
    }

    @Override
    public String[] getExportedItemsOrder() {
        return this.carousel.getExportedItemsOrder();
    }

    @Override
    public String getId() {
        return this.carousel.getId();
    }

    @Override
    public String getAppliedCssClasses() {
        return this.carousel.getAppliedCssClasses();
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }
}
