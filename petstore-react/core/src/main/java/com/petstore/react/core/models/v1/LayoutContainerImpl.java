package com.petstore.react.core.models.v1;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.adobe.cq.wcm.core.components.models.LayoutContainer;
import com.adobe.cq.wcm.core.components.models.ListItem;
import com.day.cq.wcm.foundation.model.responsivegrid.ResponsiveGrid;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.via.ResourceSuperType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.PostConstruct;
import java.util.*;

@Model(adaptables = SlingHttpServletRequest.class, adapters = { LayoutContainer.class,
        ComponentExporter.class }, resourceType = LayoutContainerImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class LayoutContainerImpl implements LayoutContainer {
    static final String RESOURCE_TYPE = "petstore-react/components/containerv2";

    @Self
    @Via(type = ResourceSuperType.class)
    private LayoutContainer container;

    @SlingObject
    private SlingHttpServletRequest request;

    private ResponsiveGrid responsiveGrid;

    @PostConstruct
    protected void initModel() {

        this.responsiveGrid = request.adaptTo(ResponsiveGrid.class);

    }

    public String getGridClassNames() {
        return this.responsiveGrid.getGridClassNames();
    }

    public int getColumnCount() {
        return this.responsiveGrid.getColumnCount();
    }

    public Map<String, String> getColumnClassNames() {
        return this.responsiveGrid.getColumnClassNames();
    }

    @Override
    public String getAccessibilityLabel() {
        return this.container.getAccessibilityLabel();
    }

    @Override
    public String getRoleAttribute() {
        return this.container.getRoleAttribute();
    }

    @Override
    public @NotNull List<ListItem> getItems() {
        return this.container.getItems();
    }

    @Override
    public @Nullable String getBackgroundStyle() {
        return this.container.getBackgroundStyle();
    }

    @Override
    public @NotNull Map<String, ? extends ComponentExporter> getExportedItems() {
        return this.container.getExportedItems();
    }

    @Override
    public @NotNull String[] getExportedItemsOrder() {
        return this.container.getExportedItemsOrder();
    }


    @Override
    public @Nullable String getAppliedCssClasses() {
        return this.container.getAppliedCssClasses();
    }

    @Override
    public @NotNull String getExportedType() {
        return RESOURCE_TYPE;
    }
}
