package com.petstore.react.core.models.v1;

import com.adobe.aem.spa.project.core.models.Page;
import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.hierarchy.HierarchyNodeExporter;
import com.adobe.cq.wcm.core.components.models.HtmlPageItem;
import com.adobe.cq.wcm.core.components.models.NavigationItem;
import com.adobe.cq.wcm.core.components.models.datalayer.ComponentData;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.via.ResourceSuperType;
import org.jetbrains.annotations.Nullable;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AbstractSpaPage implements Page {

    @Self
    @Via(type = ResourceSuperType.class)
    private Page delegate;

    public @Nullable String getHierarchyRootJsonExportUrl() {
        return delegate.getHierarchyRootJsonExportUrl();
    }

    public @Nullable Page getHierarchyRootModel() {
        return delegate.getHierarchyRootModel();
    }

    public String getExportedHierarchyType() {
        return delegate.getExportedHierarchyType();
    }

    public String getExportedPath() {
        return delegate.getExportedPath();
    }

    public Map<String, ? extends HierarchyNodeExporter> getExportedChildren() {
        return delegate.getExportedChildren();
    }

    public String getLanguage() {
        return this.delegate.getLanguage();
    }

    public Calendar getLastModifiedDate() {
        return this.delegate.getLastModifiedDate();
    }

    public String[] getKeywords() {
        return this.delegate.getKeywords();
    }

    public String getDesignPath() {
        return this.delegate.getDesignPath();
    }

    public String getStaticDesignPath() {
        return this.delegate.getStaticDesignPath();
    }

    public Map<String, String> getFavicons() {
        return this.delegate.getFavicons();
    }

    public String getTitle() {
        return this.delegate.getTitle();
    }

    public String getBrandSlug() {
        return this.delegate.getBrandSlug();
    }

    public String[] getClientLibCategories() {
        return this.delegate.getClientLibCategories();
    }

    public String[] getClientLibCategoriesJsBody() {
        return this.delegate.getClientLibCategoriesJsBody();
    }

    public String[] getClientLibCategoriesJsHead() {
        return this.delegate.getClientLibCategoriesJsHead();
    }

    public String getTemplateName() {
        return this.delegate.getTemplateName();
    }

    public String getAppResourcesPath() {
        return this.delegate.getAppResourcesPath();
    }

    public String getCssClassNames() {
        return this.delegate.getCssClassNames();
    }

    public NavigationItem getRedirectTarget() {
        return this.delegate.getRedirectTarget();
    }

    public boolean hasCloudconfigSupport() {
        return this.delegate.hasCloudconfigSupport();
    }

    public Set<String> getComponentsResourceTypes() {
        return this.delegate.getComponentsResourceTypes();
    }

    public String[] getExportedItemsOrder() {
        return this.delegate.getExportedItemsOrder();
    }

    public Map<String, ? extends ComponentExporter> getExportedItems() {
        return this.delegate.getExportedItems();
    }

    public String getId() {
        return this.delegate.getId();
    }

    public ComponentData getData() {
        return this.delegate.getData();
    }

    public String getAppliedCssClasses() {
        return this.delegate.getAppliedCssClasses();
    }

    public String getExportedType() {
        return this.delegate.getExportedType();
    }

    public String getMainContentSelector() {
        return this.delegate.getMainContentSelector();
    }

    public List<HtmlPageItem> getHtmlPageItems() {
        return this.delegate.getHtmlPageItems();
    }
}
