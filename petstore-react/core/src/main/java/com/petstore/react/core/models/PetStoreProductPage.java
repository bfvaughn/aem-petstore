package com.petstore.react.core.models;

public interface PetStoreProductPage extends PetStorePage {

    String getProductId();

    String getProductName();

    String getProductDescription();
}
