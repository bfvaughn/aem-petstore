package com.petstore.react.core.models;

import com.adobe.aem.spa.project.core.models.Page;

import java.io.IOException;

public interface PetStorePage extends Page {

    String getPageTitle();

    String getDescription();

    String getRobots();

    String getPageMetadata() throws IOException;
}
