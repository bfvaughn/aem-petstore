package com.petstore.react.core.models.v1;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.adobe.cq.wcm.core.components.commons.link.Link;
import com.adobe.cq.wcm.core.components.models.Button;
import com.petstore.base.core.models.helper.PetStoreLink;
import com.petstore.base.core.models.helper.PetStoreLinkBuilder;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.via.ResourceSuperType;

import javax.annotation.PostConstruct;

@Model(adaptables = SlingHttpServletRequest.class, adapters = { Button.class,
        ComponentExporter.class }, resourceType = ButtonImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class ButtonImpl implements Button {
    static final String RESOURCE_TYPE = "petstore-react/components/button";

    @Self
    private SlingHttpServletRequest request;

    @Self
    @Via(type = ResourceSuperType.class)
    private Button button;

    @ValueMapValue
    private String link;

    private String url;

    @PostConstruct
    private void initModel() {
        PetStoreLink petStoreLink = (PetStoreLink)getButtonLink();
        this.routed = petStoreLink.isRouted();
        this.url = petStoreLink.getUrl();
    }

    private boolean routed;

    public String getText() {
        return button != null ? button.getText() : null;
    }

    public String getIcon() {
        return button != null ? button.getIcon() : null;
    }

    public String getAccessibilityLabel() {
        return button != null ? button.getAccessibilityLabel() : null;
    }

    @Override
    public Link getButtonLink() {
        PetStoreLink build = new PetStoreLinkBuilder(request.getResource())
                .withPath(this.link)
                .withTitle(this.button.getText())
                .build();
        return build;
    }

    public String getUrl() {
        return this.url;
    }

    public String getId() {
        return button != null ? button.getId() : null;
    }

    public boolean isRouted() {
        return routed;
    }

    @Override
    public String getAppliedCssClasses() {
        return this.button.getAppliedCssClasses();
    }

    public String getExportedType() {
        return RESOURCE_TYPE;
    }

}