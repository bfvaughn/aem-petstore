package com.petstore.react.core.models;

import com.adobe.cq.wcm.core.components.models.ListItem;
import com.adobe.cq.wcm.core.components.models.Teaser;

import java.util.List;

public interface Feature extends Teaser {

    String getImagePath();

    @Override
    boolean isActionsEnabled();

    @Override
    List<ListItem> getActions();

    @Override
    String getPretitle();

    @Override
    String getTitle();

    @Override
    String getDescription();
}
