package com.petstore.react.core.listeners;

import org.apache.sling.api.resource.observation.ResourceChange;
import org.apache.sling.api.resource.observation.ResourceChangeListener;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Component(
    service = ResourceChangeListener.class,
    configurationPolicy = ConfigurationPolicy.IGNORE,
    property = {
        ResourceChangeListener.PATHS + "=/content/petstore-react",
        ResourceChangeListener.CHANGES + "=ADDED",
        ResourceChangeListener.CHANGES + "=CHANGED",
        ResourceChangeListener.CHANGES + "=REMOVED"
    })
public class PetstoreChangeListener implements ResourceChangeListener {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void onChange(List<ResourceChange> list) {
        logger.info("..... changes:");
        for (ResourceChange item : list) {
            logger.info(" Changed item: " + item.getType());
            logger.info(" Changed path: " + item.getPath());
        }
    }
}
