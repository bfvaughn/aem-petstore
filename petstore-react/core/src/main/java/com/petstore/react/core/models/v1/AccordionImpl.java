package com.petstore.react.core.models.v1;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.adobe.cq.wcm.core.components.models.Accordion;
import com.adobe.cq.wcm.core.components.models.ListItem;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.via.ResourceSuperType;

import java.util.List;
import java.util.Map;

@Model(adaptables = SlingHttpServletRequest.class, adapters = {Accordion.class,
        ComponentExporter.class}, resourceType = AccordionImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class AccordionImpl implements Accordion {
    static final String RESOURCE_TYPE = "petstore-react/components/accordion/v1/accordion";

    @Self
    @Via(type = ResourceSuperType.class)
    private Accordion accordion;
    
    @Override
    public boolean isSingleExpansion() {
        return this.accordion.isSingleExpansion();
    }

    @Override
    public String[] getExpandedItems() {
        return this.accordion.getExpandedItems();
    }

    @Override
    public String getHeadingElement() {
        return this.accordion.getHeadingElement();
    }

    @Override
    public List<ListItem> getItems() {
        return this.accordion.getItems();
    }

    @Override
    public String getBackgroundStyle() {
        return this.accordion.getBackgroundStyle();
    }

    @Override
    public Map<String, ? extends ComponentExporter> getExportedItems() {
        return this.accordion.getExportedItems();
    }

    @Override
    public String[] getExportedItemsOrder() {
        return this.accordion.getExportedItemsOrder();
    }

    @Override
    public String getId() {
        return this.accordion.getId();
    }

    @Override
    public String getAppliedCssClasses() {
        return this.accordion.getAppliedCssClasses();
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }
}
