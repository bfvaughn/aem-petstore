package com.petstore.react.core.models.v1;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.adobe.cq.wcm.core.components.models.Separator;
import com.adobe.cq.wcm.core.components.models.datalayer.ComponentData;
import com.petstore.base.core.models.helper.ContentPolicyUtil;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.via.ResourceSuperType;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Model(adaptables = SlingHttpServletRequest.class, adapters = {Separator.class,
        ComponentExporter.class}, resourceType = SeparatorImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class SeparatorImpl implements Separator {
    static final String RESOURCE_TYPE = "petstore-react/components/separator";

    @Self
    @Via(type = ResourceSuperType.class)
    private Separator separator;

    @SlingObject
    private SlingHttpServletRequest request;

    @ValueMapValue(name = "cq:styleIds")
    private List<String> styleIds;

    private String appliedCssClasses;

    @PostConstruct
    public void initModel() {

        Map<String, String> styles = ContentPolicyUtil.getComponentStyles(request.getResource());

        StringBuilder stringBuilder = new StringBuilder();
        if (styleIds != null && styleIds.size() > 0) {
            for (String styleId : styleIds) {
                if (styles.containsKey(styleId)) {
                    stringBuilder.append(styles.get(styleId));
                }
            }
            this.appliedCssClasses = stringBuilder.toString();
        }

    }

    @Override
    public String getId() {
        return this.separator.getId();
    }

    @Override
    public ComponentData getData() {
        return this.separator.getData();
    }

    @Override
    public String getAppliedCssClasses() {
        return this.appliedCssClasses;

    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }
}
