package com.petstore.react.core.workflow;

import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;
import com.adobe.granite.security.user.UserPropertiesService;
import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component(property = {
        Constants.SERVICE_DESCRIPTION + "=Email notification process step",
        Constants.SERVICE_VENDOR + "=Slalom",
        "process.label" + "=Email Notification Process"
})
public class EmailNotificationProcess implements WorkflowProcess {
    final Logger logger = LoggerFactory.getLogger(getClass());

    private static final String REVIEW_TMP = "/conf/petstore-react/settings/workflow/notification/email/review.txt";
    private static final String APPROVE_TMP = "/conf/petstore-react/settings/workflow/notification/email/approval.txt";
    private static final String REJECT_TMP = "/conf/petstore-react/settings/workflow/notification/email/rejected.txt";

    @Reference
    private MessageGatewayService messageGatewayService;
    @Reference
    private UserPropertiesService userPropertiesService;
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    private String PROCESS_ARGS = "PROCESS_ARGS";

    @Override
    public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaDataMap) throws WorkflowException {

        String processArgs = metaDataMap.get(PROCESS_ARGS, String.class);
        logger.info("Found processArgs {$1} ", processArgs);
        SimpleEmail email = null;

        try {
            if ("review".equals(processArgs)) {
                logger.info("Send review workflow email");
                email = buildReviewEmail(workflowSession);
            } else if ("approved".equals(processArgs)) {
                logger.info("Send approval email ");
                email = buildApprovalEmail(workflowSession);
            } else if ("rejected".equals(processArgs)) {
                logger.info("Send rejected email " );
                email = buildRejectionEmail(workflowSession);
            } else {
                logger.info("Failed to match process {$1} ", processArgs);
            }

            if (email != null) {
                sendEmail(workflowSession, workItem, email);
            }


        } catch (Exception e) {
            logger.error("Failed to send email", e);
        }


    }

    private SimpleEmail buildReviewEmail(WorkflowSession workflowSession) throws EmailException, MessagingException, IOException {
        MailTemplate mailTemplate = MailTemplate.create(REVIEW_TMP, workflowSession.getSession());
        SimpleEmail email = null;
        email = mailTemplate.getEmail(StrLookup.mapLookup(new HashMap()), SimpleEmail.class);
        email.setSubject("Page Modifications Review Requested");
        return email;
    }

    private SimpleEmail buildApprovalEmail(WorkflowSession workflowSession) throws EmailException, MessagingException, IOException {
        MailTemplate mailTemplate = MailTemplate.create(APPROVE_TMP, workflowSession.getSession());
        SimpleEmail email = null;
        email = mailTemplate.getEmail(StrLookup.mapLookup(new HashMap()), SimpleEmail.class);
        email.setSubject("Page Modifications Approved");
        return email;
    }

    private SimpleEmail buildRejectionEmail(WorkflowSession workflowSession) throws EmailException, MessagingException, IOException {
        MailTemplate mailTemplate = MailTemplate.create(REJECT_TMP, workflowSession.getSession());
        SimpleEmail email = null;
        email = mailTemplate.getEmail(StrLookup.mapLookup(new HashMap()), SimpleEmail.class);
        email.setSubject("Page Modifications Rejected");
        return email;
    }

    private void sendEmail(WorkflowSession workflowSession, WorkItem workItem, SimpleEmail email) throws EmailException, RepositoryException, LoginException {

        MessageGateway<SimpleEmail> messageGateway = messageGatewayService.getGateway(SimpleEmail.class);
        String emailAddress = getEmailAddress(workflowSession, workItem);
        email.addTo(emailAddress);
        messageGateway.send(email);

    }

    private String getEmailAddress(WorkflowSession workflowSession, WorkItem workItem) {
        String emailAddress = null;
        final Map<String, Object> authInfo = new HashMap<String, Object>();
        authInfo.put(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, workflowSession.getSession());
        try (ResourceResolver resourceResolver = resourceResolverFactory.getResourceResolver(authInfo) ) {
            UserPropertiesManager userPropertiesManager = userPropertiesService.createUserPropertiesManager(resourceResolver);

            String initiator = workItem.getWorkflow().getInitiator();
            UserProperties properties = userPropertiesManager.getUserProperties(initiator, "profile");
            emailAddress = properties.getProperty("email");
        } catch (LoginException | RepositoryException e) {
            logger.error("Failed to get user email address", e);
        }


        return emailAddress;
    }
}
