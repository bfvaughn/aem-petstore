package com.petstore.react.core.models.v1;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.adobe.cq.wcm.core.components.commons.link.Link;
import com.adobe.cq.wcm.core.components.models.ListItem;
import com.adobe.cq.wcm.core.components.models.Teaser;
import com.petstore.react.core.models.Feature;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.via.ResourceSuperType;

import javax.annotation.PostConstruct;
import java.util.List;

@Model(adaptables = SlingHttpServletRequest.class, adapters = { Feature.class,
        ComponentExporter.class }, resourceType = FeatureImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class FeatureImpl implements Feature {
    static final String RESOURCE_TYPE = "petstore-react/components/feature";

    @Self
    @Via(type = ResourceSuperType.class)
    private Teaser teaser;

    private String imagePath;

    @PostConstruct
    protected void init() {
        if (this.teaser != null && this.teaser.getImageResource() != null) {
            this.imagePath = this.teaser.getImageResource().getValueMap().get("fileReference", String.class);
        }
    }

    @Override
    public String getImagePath() {
        return this.imagePath;
    }

    @Override
    public boolean isActionsEnabled() {
        return this.teaser.isActionsEnabled();
    }

    @Override
    public List<ListItem> getActions() {
        return this.teaser.getActions();
    }

    @Override
    public Link getLink() {
        return this.teaser.getLink();
    }

    @Override
    public String getLinkURL() {
        return this.teaser.getLinkURL();
    }

    @Override
    public String getPretitle() {
        return this.teaser.getPretitle();
    }

    @Override
    public String getTitle() {
        return this.teaser.getTitle();
    }

    @Override
    public boolean isTitleLinkHidden() {
        return this.teaser.isTitleLinkHidden();
    }

    @Override
    public String getDescription() {
        return this.teaser.getDescription();
    }

    @Override
    public String getTitleType() {
        return this.teaser.getTitleType();
    }

    @Override
    public String getId() {
        return this.teaser.getId();
    }


    @Override
    public String getAppliedCssClasses() {
        return this.teaser.getAppliedCssClasses();
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }
}
