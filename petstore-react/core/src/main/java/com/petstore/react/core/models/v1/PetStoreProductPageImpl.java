package com.petstore.react.core.models.v1;

import com.adobe.aem.spa.project.core.models.Page;
import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.petstore.react.core.models.PetStoreProductPage;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.via.ResourceSuperType;

import javax.annotation.PostConstruct;

@Model(adaptables = SlingHttpServletRequest.class,
        adapters = { Page.class, PetStoreProductPage.class, ComponentExporter.class },
        resourceType = PetStoreProductPageImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class PetStoreProductPageImpl extends PetStorePageImpl implements PetStoreProductPage {
    static final String RESOURCE_TYPE = "petstore-react/components/product-page";

    @ScriptVariable
    @Via(type = ResourceSuperType.class)
    private com.day.cq.wcm.api.Page currentPage;

    @Self
    private SlingHttpServletRequest request;

    private String productId;

    private String productName;

    private String productDescription;

    @PostConstruct
    public void initModel() {
        ValueMap pageProperties = this.currentPage.getProperties();

        String selectors = request.getRequestPathInfo().getSelectorString();
        if (selectors != null && selectors.contains(".")) {
            String[] product = selectors.split("\\.");
            if (product.length == 3) {
                this.productId = product[1];
                this.productName = product[2];
            }
        }
    }

    public String getWhoAMI() {
        return this.getClass().getName();
    }

    @Override
    public String getProductId() {
        return productId;
    }

    @Override
    public String getProductName() {
        return this.productName;
    }

    @Override
    public String getProductDescription() {
        return this.productDescription;
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }

    public String getExportedPath() {
        String exportedPath = this.currentPage.getPath();
        //TODO Add code to append selector
        if (this.request.getRequestPathInfo().getSelectors().length > 1) {
            String selectors = this.request.getRequestPathInfo().getSelectorString().replace("model", "");
            exportedPath += selectors;
        }
        return exportedPath;
    }
}
