package com.petstore.react.core.models;

import com.adobe.cq.wcm.core.components.models.ListItem;
import com.adobe.cq.wcm.core.components.models.Teaser;
import org.apache.sling.api.resource.Resource;

import java.util.List;

public interface Card extends Teaser {

    String getIconImage();

    @Override
    boolean isActionsEnabled();

    @Override
    List<ListItem> getActions();

    @Override
    Resource getImageResource();

    @Override
    String getTitle();

    @Override
    String getDescription();
}
