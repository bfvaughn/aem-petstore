package com.petstore.react.core.models.v1;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.adobe.cq.wcm.core.components.commons.link.Link;
import com.adobe.cq.wcm.core.components.models.Title;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.*;
import org.apache.sling.models.annotations.via.ResourceSuperType;

import javax.annotation.PostConstruct;

@Model(adaptables = SlingHttpServletRequest.class, adapters = { Title.class,
        ComponentExporter.class }, resourceType = TitleImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class TitleImpl implements Title {
    static final String RESOURCE_TYPE = "petstore-react/components/title";

    @Self
    @Via(type = ResourceSuperType.class)
    private Title title;

    @ValueMapValue(name = "jcr:title"
    )
    private String authoredTitle;

    @ValueMapValue
    private String linkURL;

    @SlingObject
    private SlingHttpServletRequest request;

    @ScriptVariable
    private PageManager pageManager;

    private Page currentPage;

    private Page linkedPage;

    @PostConstruct
    public void initModel() {
        this.currentPage = pageManager.getContainingPage(request.getResource().getPath());
        if (StringUtils.isNotEmpty(this.linkURL)) {
            linkedPage = pageManager.getPage(this.linkURL);
        }
    }

    @Override
    public String getText() {
        String text = null;
        if (StringUtils.isEmpty(authoredTitle)) {
            text = currentPage.getTitle();
            if (linkedPage != null) {
                text = linkedPage.getTitle();
            }
        } else {
            text = authoredTitle;
        }

        return text;
    }

    @Override
    public String getType() {
        return this.title.getType();
    }

    @Override
    public Link getLink() {
        return this.title.getLink();
    }

    @Override
    public String getAppliedCssClasses() {
        return this.title.getAppliedCssClasses();
    }

    @Override
    public boolean isLinkDisabled() {
        return this.linkedPage == null;
    }

    @Override
    public  String getExportedType() {
        return this.RESOURCE_TYPE;
    }
}
