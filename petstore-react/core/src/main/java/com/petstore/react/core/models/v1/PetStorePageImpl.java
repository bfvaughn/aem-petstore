package com.petstore.react.core.models.v1;

import com.adobe.aem.spa.project.core.models.Page;
import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.petstore.react.core.models.PetStorePage;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.via.ResourceSuperType;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Model(adaptables = SlingHttpServletRequest.class,
        adapters = { Page.class, PetStorePage.class, ComponentExporter.class },
        resourceType = PetStorePageImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class PetStorePageImpl extends AbstractSpaPage implements PetStorePage {
    static final String RESOURCE_TYPE = "petstore-react/components/page";
    
    @ScriptVariable
    @Via(type = ResourceSuperType.class)
    private com.day.cq.wcm.api.Page currentPage;

    private boolean noIndex;

    private boolean noFollow;
    
    @PostConstruct
    public void initModel() {
        ValueMap pageProperties = this.currentPage.getProperties();
        if (pageProperties != null) {
            this.noFollow = pageProperties.get("noFollow", false);
            this.noIndex = pageProperties.get("noIndex", false);
        }
    }

    public String getWhoAMI() {
        return this.getClass().getName();
    }

    @Override
    public String getPageTitle() {
        return this.currentPage.getPageTitle();
    }

    @Override
    public String getDescription() {
        return this.currentPage.getDescription();
    }

    @Override
    public String getRobots() {
        StringBuilder robots = new StringBuilder();
        if (noIndex) {
            robots.append("noIndex,");
        } else {
            robots.append("index,");
        }

        if (noFollow) {
            robots.append(" noFollow");
        } else {
            robots.append(" follow");
        }

        return robots.toString();
    }

    @Override
    public String getPageMetadata() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> metadata = new HashMap<>();
        metadata.put("@context", "http://schema.org");
        metadata.put("@type", "http://schema.org");
        metadata.put("name", getPageTitle());
        metadata.put("description", getDescription());
        metadata.put("url", "https://www.petsore.com/react");

        return mapper.writeValueAsString(metadata);

    }
}
