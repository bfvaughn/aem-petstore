Time: ${event.TimeStamp}
Step: ${item.node.title}
User: ${participant.name} (${participant.id})
Workflow: ${model.title}

Your page changes have been approved and published.

View the overview in your ${host.prefix}/aem/inbox

This is an automatically generated message. Please do not reply.