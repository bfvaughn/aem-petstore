import React from 'react';
import {MapTo} from '@adobe/aem-react-editable-components';
import {Link} from "react-router-dom";
import getI18nText from '../../../utils/GranteI18nHelper';

require('./Button.scss');

export const ButtonEditConfig = {
    emptyLabel: 'Button',
    isEmpty: function(props) {
        return !props || !props.url || props.url.trim().length < 1;
    }
};

interface ButtonModel {
    url?: string;
    routed?: boolean;
    text?: string;
    id?: string;
}

const ButtonHref = (props:ButtonModel) => {
  return (
     <div className="button"  >
       <a id={props.id} className="cmp-button" data-cmp-clickable="" href={props.url} >
         <span className="cmp-button__text">{ getI18nText(props.text) }</span>
       </a>
     </div>
  )
};

const ButtonRouted = (props:ButtonModel) => {
  return (
     <div className="button"  >
       <Link to={props.url} className="cmp-button">
         <span className="cmp-button__text">{ getI18nText(props.text) }</span>
       </Link>
     </div>
  )
};

const Button = (props:ButtonModel) => {
      return (props.routed) ? <ButtonRouted {...props}/> : <ButtonHref {...props}/>;
};

export default Button;

MapTo('petstore-react/components/button')(Button, ButtonEditConfig);