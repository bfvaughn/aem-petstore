import React, {Component} from 'react';
import {MapTo} from '@adobe/aem-react-editable-components';
import {TitleV2} from '@adobe/aem-core-components-react-base';
import Text from  '../../Text/v1/Text';
import Image from  '../../Image/v1/Image';
import Button from  '../../Button/v1/Button';

require('./Feature.scss');

export const FeatureEditConfig = {
    emptyLabel: 'Feature',
    isEmpty: function(props) {
        return !props || !props.title || props.title.trim().length < 1;
    }
};

export default class Feature extends Component {

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="cmp-feature-col-left col-sm-12 col-lg-6">
            <Image src={this.props.imagePath} > </Image>
          </div>
          <div className="cmp-feature-col-right col-sm-12 col-lg-6">
            <TitleV2 text={this.props.title} linkDisabled={true} ></TitleV2>
            <Text text={this.props.description} richText={true} ></Text>
            <Button routed={true} url={"/content/petstore-react/us/en/home/dogs/dog-toys.html"} text={"Dog Toys"}> </Button>
          </div>
        </div>
      </div>
    );
  }
}

MapTo('petstore-react/components/feature')(Feature, FeatureEditConfig);