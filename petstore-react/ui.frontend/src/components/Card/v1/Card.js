import React from 'react';
import {MapTo} from '@adobe/aem-react-editable-components';
import {TitleV2} from '@adobe/aem-core-components-react-base';
import Text from  '../../Text/v1/Text';
import Button from  '../../Button/v1/Button';
import {Link} from "react-router-dom";

require('./Card.scss');

export const CardEditConfig = {
    emptyLabel: 'Card-V1',
    isEmpty: function(props) {
        return !props || !props.title || props.title.trim().length < 1;
    }
};

export interface ActionModel {
    url?: string;
    title?: string;
    id?: string;
}

export interface ImageModel {
    fileReference?: string;
    iconImage?: boolean;
    id?: string;
}

export interface CardModel {
    title?: string;
    actionsEnabled?: boolean;
    description?: string;
    id?: string;
    actions?: ActionModel[];
    imageResource: ImageModel;
}

const generateLink = (action:ActionModel, index:number) => {
    return <Button key={"link-" + index} routed={true} url={action.url} text={action.title}> </Button>;
}

const CardButtons = (props:CardModel) => {
  const showActions:boolean = ( props.actions.length > 0 ) && props.actionsEnabled;

  if ( showActions ) {
    return (
      <div className="card-buttons">
        {
          props.actions.map((action, index) => {
              return generateLink(action,index)
          })
        }
      </div>
    );
  } else {
    return null;
  }
}

const CardDefault = (props:CardModel) => {
  console.log("...card CardDefault 1");
  return (
    <div className="container">
      <div className="card-container">
        <div className="card-content">
          <TitleV2 text={props.title} linkDisabled={true} ></TitleV2>
          <Text text={props.description} richText={true} ></Text>
          <CardButtons {...props}/>
        </div>
      </div>
    </div>
  )
};

const CardLink = (props:CardModel) => {
  console.log("...card CardLink 2");
  return (
    <div className="container">
      <a href={props.linkURL}>
        <div className="card-container">
          <div className="card-content">
            <TitleV2 text={props.title} linkDisabled={true} ></TitleV2>
            <Text text={props.description} richText={true} ></Text>
            <CardButtons {...props}/>
          </div>
        </div>
      </a>
    </div>
  )
};

const CardWithIcon = (props:CardModel) => {
  console.log("...card CardWithIcon 3");
  return (
    <div className="container">
      <div className="card-container">
        <div className="card-icon"
            style={{ backgroundImage: `url(${props.imageResource.fileReference})` }}>
        </div>
        <div className="card-content">
          <TitleV2 text={props.title} linkDisabled={true} ></TitleV2>
          <Text text={props.description} richText={true} ></Text>
          <CardButtons {...props}/>
        </div>
      </div>
    </div>
  )
};

const CardLinkWithIcon = (props:CardModel) => {
  console.log("...card CardLinkWithIcon 4");
  return (
    <div className="container">
      <Link to={props.linkURL}>
        <div className="card-container">
          <div className="card-icon"
              style={{ backgroundImage: `url(${props.imageResource.fileReference})` }}>
          </div>
          <div className="card-content">
            <TitleV2 text={props.title} linkDisabled={true} ></TitleV2>
            <Text text={props.description} richText={true} ></Text>
          </div>
        </div>
      </Link>
    </div>
  )
}

const CardImage = (props:CardModel) => {
  console.log("...card CardImage 5");
  return (
    <div className="container image-card container-dark card-dark"
        style={{ backgroundImage: `url(${props.imageResource.fileReference})` }}>
      <div className="card-container">
        <div className="card-content">
          <TitleV2 text={props.title} linkDisabled={true} ></TitleV2>
          <Text text={props.description} richText={true} ></Text>
          <CardButtons {...props}/>
        </div>
      </div>
    </div>
  )
};

const Card = (props:CardModel) => {

  if (props.imageResource ) {
    if (props.imageResource.iconImage === 'true') {
      if (props.linkURL && props.actionsEnabled === false) {
        return <CardLinkWithIcon {...props}/>;
      } else {
        return <CardWithIcon {...props}/>;
      }
    } else {
      return <CardImage {...props}/>;
    }
  } else {
    if (props.linkURL && props.actionsEnabled === false) {
      return <CardLink {...props}/>;
    } else {
      return <CardDefault {...props}/>;
    }
  }

};

export default Card;

MapTo('petstore-react/components/card')(Card, CardEditConfig);