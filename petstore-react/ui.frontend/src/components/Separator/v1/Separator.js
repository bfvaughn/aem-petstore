import React from 'react';
import {MapTo} from '@adobe/aem-react-editable-components';

require('./Separator.scss');

export const SeparatorEditConfig = {
  emptyLabel: 'Separator',
  isEmpty: function(props) {
    return !props;
  }
};

const Separator = () => {
  return (
    <div className="separator">
      <div className="cmp-separator">
        <hr className="cmp-separator__horizontal-rule"></hr>
      </div>
    </div>
  )
};

MapTo('petstore-react/components/separator')(Separator, SeparatorEditConfig);
