/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 ~ Copyright 2020 Adobe Systems Incorporated
 ~
 ~ Licensed under the Apache License, Version 2.0 (the "License");
 ~ you may not use this file except in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~     http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing, software
 ~ distributed under the License is distributed on an "AS IS" BASIS,
 ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ~ See the License for the specific language governing permissions and
 ~ limitations under the License.
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
import withAsyncImport from "../utils/withAsyncImport";

import './Page/v1/Page';
import './ProductPage/v1/ProductPage';
import './Container/v1/Container';
import './ExperienceFragment/v1/ExperienceFragment';
import './Image/v1/Image';
import './HelloWorld/v1/HelloWorld';
import './Separator/v1/Separator';
import './Button/v1/Button';
import './Feature/v1/Feature';
import './Card/v1/Card';
import './Carousel/v1/CarouselV1';
import './Accordion/v1/AccordionV1';

import {MapTo} from '@adobe/aem-react-editable-components';

import {
    TitleV2IsEmptyFn
} from '@adobe/aem-core-components-react-base/dist/isEmptyFunctions';

import {
    ContainerV1, ContainerV1IsEmptyFn,
    TabsV1, TabsV1IsEmptyFn,
    AccordionV1,AccordionV1IsEmptyFn,
} from '@adobe/aem-core-components-react-spa';

import {
    BreadCrumbV2,BreadCrumbV2IsEmptyFn,
    LanguageNavigationV1,
    NavigationV1,
    TeaserV1,TeaserV1IsEmptyFn,
    DownloadV1,DownloadV1IsEmptyFn,
    ListV2,ListV2IsEmptyFn
} from '@adobe/aem-core-components-react-base';

//Import CSS
require('./Navigation/Navigation.scss');


//lazyload / code splitting example of an internal component
const LazyTextComponent = withAsyncImport(() => import(`./Text/v1/Text`));

//lazyload / code splitting examples of external components
const TitleV2 = withAsyncImport(() => import(`@adobe/aem-core-components-react-base/dist/authoring/title/v2/TitleV2`));

MapTo('petstore-react/components/download')(DownloadV1, {isEmpty: DownloadV1IsEmptyFn});
MapTo('petstore-react/components/list')(ListV2, {isEmpty: ListV2IsEmptyFn});
MapTo('petstore-react/components/teaser')(TeaserV1, {isEmpty: TeaserV1IsEmptyFn});
MapTo('petstore-react/components/title')(TitleV2, {isEmpty: TitleV2IsEmptyFn});


MapTo('petstore-react/components/breadcrumb')(BreadCrumbV2, {isEmpty: BreadCrumbV2IsEmptyFn});
MapTo('petstore-react/components/navigation')(NavigationV1);
MapTo('petstore-react/components/languagenavigation')(LanguageNavigationV1);


MapTo('petstore-react/components/tabs')(TabsV1, {isEmpty: TabsV1IsEmptyFn});
MapTo('petstore-react/components/accordion')(AccordionV1, {isEmpty: AccordionV1IsEmptyFn});
MapTo('petstore-react/components/container')(ContainerV1, {isEmpty: ContainerV1IsEmptyFn});
MapTo('petstore-react/components/containerv2')(ContainerV1, {isEmpty: ContainerV1IsEmptyFn});

/**
 * Default Edit configuration for the Text component that interact with the Core Text component and sub-types
 *
 * @type EditConfig
 */
const TextEditConfig = {
    emptyLabel: 'Text',

    isEmpty: function (props) {
        return !props || !props.text || props.text.trim().length < 1;
    }
};

MapTo('petstore-react/components/text')(LazyTextComponent, TextEditConfig);