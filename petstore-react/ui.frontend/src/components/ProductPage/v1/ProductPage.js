/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 ~ Copyright 2020 Adobe Systems Incorporated
 ~
 ~ Licensed under the Apache License, Version 2.0 (the "License");
 ~ you may not use this file except in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~     http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing, software
 ~ distributed under the License is distributed on an "AS IS" BASIS,
 ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ~ See the License for the specific language governing permissions and
 ~ limitations under the License.
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

import {
  MapTo,
  Page,
  withComponentMappingContext
} from '@adobe/aem-react-editable-components';
import { withRoute } from '../../RouteHelper/RouteHelper';

require('./ProductPage.scss');

// This component is a variant of a Page component mapped to the
// "petstore-react/components/page" resource type. For now, the rendering is
// the same as the RootPage; this is more for illustration purposes
class AppProductPage extends Page {
  get containerProps() {
    let attrs = super.containerProps;
    attrs.className =
      (attrs.className || '') + ' page ' + (this.props.cssClassNames || '');
    return attrs;
  }

  constructor(props: P) {
      super(props);
      console.log(props);
  }

  componentDidMount() {
    this.updatePageInfo();
  }

  updatePageInfo() {
    this.setTitle();
    this.setDescription();
    this.setRobots();
  }

  setTitle() {
    document.title = this.props.title + ' | PetStore';
    this.updateProperty("meta[property='og:title']", this.props.title);
  }

  setDescription() {
    this.updateProperty("meta[name='description']", this.props.description);
    this.updateProperty("meta[property='og:description']", this.props.description);
  }

  setRobots() {
    this.updateProperty("meta[name='robots']", this.props.robots);
  }

  updateProperty(field, value) {
    const element = document.querySelector(field);
    if (element && value) {
      element.setAttribute('content', value);
    }
  }
}

export default MapTo('petstore-react/components/product-page')(
  withComponentMappingContext(withRoute(AppProductPage))
);
