import React  from 'react';
import {MapTo} from '@adobe/aem-react-editable-components';
import getI18nText from '../../../utils/GranteI18nHelper';

export const HelloWorldEditConfig = {
    emptyLabel: 'Hello World',
    isEmpty: function(props) {
        return !props || !props.text || props.text.trim().length < 1;
    }
};

export interface HelloWorldModel {
    message?: string;
    text?: string;
}

const HelloWorld = (props:HelloWorldModel) => {
  return (
    <div>
        <p>{ getI18nText(props.text) } </p>
        <p>{ props.message } </p>
    </div>
  )
};

MapTo('petstore-react/components/helloworld/v1/helloworld')(HelloWorld, HelloWorldEditConfig);
