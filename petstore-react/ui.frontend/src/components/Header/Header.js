//Header.js
import React, {Component} from 'react';
require('./Header.scss');

export default class Header extends Component {

  render() {
    return (
      <header className="Header">
          <div className="Header-container">
              <h1>Pet Store</h1>
          </div>
      </header>
    );
  }
}
