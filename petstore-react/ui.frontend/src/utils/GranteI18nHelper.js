/**
* Helper function to access the AEM i18n translations
*/
export default function getI18nText(key) {
  if (key && window.Granite && window.Granite.I18n) {
    return window.Granite.I18n.get(key);
  }
  return key;
}