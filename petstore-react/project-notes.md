# Lessons Learned
These are lessons learned based on building React SPA using AEM Archetype version 30 
and version 6.5.8 of AEM. 

## Image Component
Needed to not use the Core Image component because the model would 
throw exceptions when the Model generated the JSON.  My class is just 
wrapping the Core Image component and calling its getter methods.

###Core Component Composition
```
public class MyImage implements Image {

    
    @Self
    @Via(type = ResourceSuperType.class)
    private Image image;
```
By utilizing the delegate pattern, your component doesn't need to
worry about capturing the inherited properties and by extending the
model interface, you keep the expected contract for the component.

## Button Component
The Core Button class is not generating Router Links correctly. To
fix this I have implemented my own Button that wraps the Core Button Component. 
I also provide a wrapper class for Link to generate the correct
router link properties.

React uses a Link class to expose routed links.  The Button component has been 
updated to check if the button is routed and render the correct html.

###Button Link Wrapping
```
    @Override
    public Link getButtonLink() {
        PetStoreLink build = new PetStoreLinkBuilder(request.getResource())
                .withPath(this.link)
                .withTitle(button.getText())
                .build();
        return build;
    }
```

###Button Model JSON
```
"button": {
    "accessibilityLabel": "button label",
    "buttonLink": {
        "url": "/content/petstore-ang/us/en/home",
        "routed": true,
        "title": "Learn more",
        "valid": true
    },
    "id": "btn-id",
    "text": "Learn more",
    "appliedCssClassNames": "btn-line",
    "dataLayer": {
        "btn-id": {
            "@type": "core/wcm/components/button/v1/button",
            "repo:modifyDate": "2021-11-12T18:09:42Z",
            "dc:title": "Learn more"
        }
    },
    ":type": "petstore-ang/components/button"
}
```

##Style System
The style system works for most components but not all.  I needed to create a 
ContentPolicyUtil class to retrieve the styles that can be applied to a component.
The component can then look through the applied styles to get the correct class names
to apply to the component.

##Styling Components
I was not able to get the React project to work with scss.  I did create a base css 
file for the global style changes.  For components, you need to use a require syntax 
to include the css files.  This is defined in the js files.  For componnets without a js file, 
I included it in the import-components.js file.

```
require('./Navigation/Navigation.css');
```

###Converting to SCSS
Node SCSS was not installing correctly for me with the React project. I 
ended up switching to use just SCSS.  I found this referenced in another 
AEM React project developed by Adobe.

Install SCSS in ui.frontend
```
npm install scss
```
Update package.json to include:
```
"sass": "^1.42.1"
```

##Mapping AEM components to React components
As of Archetype 30, ui.frontend is using import-components.js to contain the code to 
include components and css.  It does not see to work if I use the mapTo method inside 
the import-component.js file.  I have been doing the mapTo inside the component js file.

```
import './Button/Button';
```

###Using i18n in React
Needed to build a function to call window.Granite.I18n.get(key) to get the translation key.  Also needed to add 
the granite.utils to the clientlibs-base dependencies in .context.xml file.